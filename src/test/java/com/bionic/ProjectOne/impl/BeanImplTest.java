package com.bionic.ProjectOne.impl;

import junit.framework.TestCase;
import com.bionic.ProjectOne.Bean;

public class BeanImplTest extends TestCase {

    public void testBeanIsABean() {
	Bean aBean = new BeanImpl();
        assertTrue(aBean.isABean());
    }

}